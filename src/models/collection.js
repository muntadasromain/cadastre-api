import { v4 as uuidv4 } from "uuid";

class Collection {
  constructor(name, author, description, plots, allowedUsers) {
    this.uuid = uuidv4();
    this.name = name;
    this.author = author;
    this.description = description;
    this.creationDate = new Date();
    this.lastEdited = new Date();
    this.plots = plots;
    this.allowerUsers = allowedUsers ? allowedUsers : [];
  }

  toJSON() {
    return {
      uuid: this.uuid,
      name: this.name,
      author: this.author,
      description: this.description,
      creationDate: this.creationDate,
      lastEdited: this.lastEdited,
      plots: this.plots,
      allowedUsers: this.allowedUsers,
    };
  }
}

module.exports = Collection;
