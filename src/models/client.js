import { v4 as uuidv4 } from "uuid";

class Client {
  constructor(email, name, firstName, sub) {
    this.uuid = uuidv4();
    this.email = email;
    this.name = name;
    this.firstName = firstName;
    this.sub = sub;
    this.creationDate = new Date();
  }

  toJSON() {
    return {
      uuid: this.uuid,
      email: this.email,
      name: this.name,
      firstName: this.firstName,
      sub: this.sub,
      creationDate: this.creationDate,
    };
  }
}

module.exports = Client;
