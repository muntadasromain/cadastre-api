const express = require("express");
const router = express.Router();
const { validateAccessToken } = require("../middleware/auth0.middleware");
const userController = require("../controllers/user-controller");
const jwt = require("jsonwebtoken");

// Middleware function to handle "/api/user/:userUUID" path
router.use("/api/users/", validateAccessToken, function (req, res, next) {
  try {

    let userSub = jwt.decode(req.auth.token).sub;
    userController.getUserBySub(req, res, userSub).then((client) => {
      if (client) {
        req.authUser = client;
        next();
      } else if (req.path === "/check") {
        next();
      } else if (req.path === "/" && req.method === "POST") {
        next();
      } else {
        res.status(401).send("Unauthorized");
      }
    });
  } catch (error) {
    console.log(error);
  }
});

// Specific routes for "/api/user/:userUUID" path

// get self
router.get("/api/users/self", (req, res) => {
  console.log(req.authUser);
  res.status(200).json(req.authUser);
});

// GET the user
router.get("/api/users/:userUUID", userController.getUserInfos);

// update the user
router.put("/api/users/:userUUID", (req, res) => {
  console.log(req.body);
  res.status(200).json(req.body);
});

router.post("/api/users/check", userController.checkUser);

router.get("/api/users", userController.getAllUsers);

router.post("/api/users", userController.createAccount);

module.exports = router;
