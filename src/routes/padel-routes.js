const express = require("express");
const router = express.Router();

router.get("/api/padel/prices-pie", function (req, res) {
    const padelCon = req.app.get('padelConnection');

    try {
        padelCon.query("select count(court.id) as nbCourts, court.price from court group by court.price", (err, rows) => {
            if (err) {
                res.status(500).send("Error");
                console.log(err);
            } else {
                // for each collection, set the plots value to its JSON.parse() value
                res.status(200).json(rows);
            }
        })
    } catch (error) {
        console.log(error)
    }
})

module.exports = router