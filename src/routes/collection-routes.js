const express = require("express");
const router = express.Router();
const { validateAccessToken } = require("../middleware/auth0.middleware");
const jwt = require("jsonwebtoken");
const {
  getOwnCollections,
  getSharedCollections,
  getCollection,
  createCollection,
  updateCollection,
  deleteCollection,
  updateCollectionUsers,
  updateCollectionName,
  updateCollectionDescription,
  updateCollectionFav,
} = require("../controllers/collection-controller");
const { getUserBySub } = require("../controllers/user-controller");

// Middleware function to handle "/api/user/:userUUID" path
router.use("/api/collections/", validateAccessToken, function (req, res, next) {
  try {

    let userSub = jwt.decode(req.auth.token).sub;
    getUserBySub(req, res, userSub).then((client) => {
      if (client) {
        req.authUser = client;
        next();
      } else {
        res.status(401).send("Unauthorized");
      }
    });
  } catch (error) {
    console.log(error);
  }
});

router.get("/api/collections/own", getOwnCollections);

router.get("/api/collections/shared", getSharedCollections);

router.get("/api/collections/:collectionUUID", getCollection);

router.post("/api/collections", createCollection);

router.put("/api/collections/:collectionUUID", updateCollection);

router.put("/api/collections/:collectionUUID/users", updateCollectionUsers);
router.put("/api/collections/:collectionUUID/name", updateCollectionName);
router.put(
  "/api/collections/:collectionUUID/description",
  updateCollectionDescription
);
router.put("/api/collections/:collectionUUID/fav", updateCollectionFav);

router.delete("/api/collections/:collectionUUID", deleteCollection);

module.exports = router;
