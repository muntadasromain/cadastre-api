const express = require("express");
const router = express.Router();

const dvfController = require("../controllers/dvf-controller");

router.use("/api/dvf/", function (req, res, next) {
    next();
});

router.get("/api/dvf/plots/:plotId", dvfController.getDVFByPlotId);
router.get("/api/dvf/sections/:plotId", dvfController.getDVFBySection);
router.get("/api/dvf/:insee", dvfController.getDVFByInsee);
router.get("/api/dvf/:insee/:year", dvfController.getDVFByInseeByYear);

module.exports = router;
