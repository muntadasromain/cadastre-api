const express = require("express");
const router = express.Router();

const dataController = require("../controllers/data-controller");

router.use("/api/data/", function (req, res, next) {
  next();
});

router.get("/api/data/test", (req, res) => {
  dataController.fetchParcellesByDpt("01").then((data) => {
    console.log(data);
    res.status(200).json(data);
  });
});

router.get("/api/data/sync", (req, res) => {
  dataController.synchronizeData();
  res.status(200).json("Synchronisation démarrée...");
});

module.exports = router;
