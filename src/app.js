const express = require("express");
const dotenv = require("dotenv");
const helmet = require("helmet");
const nocache = require("nocache");
const cors = require("cors");
const mysql = require("mysql");
const { auth } = require("express-oauth2-jwt-bearer");
const { validateAccessToken } = require("./middleware/auth0.middleware");
const { errorHandler } = require("./middleware/error.middleware");
const { notFoundHandler } = require("./middleware/not-found.middleware");
const bodyParser = require("body-parser");
const https = require('https');
const fs = require('fs');
const fetch = require("node-fetch");
globalThis.fetch = fetch;

const hostname = "app.idelia.org"

const userRoutes = require("./routes/user-routes");
const collectionRoutes = require("./routes/collection-routes");
const dataRoutes = require("./routes/data-routes");
const dvfRoutes = require("./routes/dvf-routes")
const padelRoutes = require("./routes/padel-routes")

dotenv.config();

if (!(process.env.PORT && process.env.CLIENT_ORIGIN_URL)) {
  throw new Error(
    "Missing required environment variables. Check docs for more info."
  );
}

const PORT = parseInt(process.env.PORT, 10);
const CLIENT_ORIGIN_URL = process.env.CLIENT_ORIGIN_URL;

// Connection MySQL

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: "idelia",
  password: "Hostin@31180",
  database: "cadastre",
  port: 3306,
});

const padelConnection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: "idelia",
  password: "Hostin@31180",
  database: "padelstats",
  port: 3306
})

connection.connect((err) => {
  if (err) {
    console.log("Error connecting to Db");
  } else {
    console.log("Connection established");
  }
});

padelConnection.connect((err) => {
  if (err) {
    console.log("Error connecting to padel db")
  } else {
    console.log("connected to padel db")
  }
})

// Routes

const app = express();
const router = express.Router();

app.use(express.json({ limit: "10mb" }));
app.use(express.urlencoded({ limit: "10mb", extended: true }));
app.set("json spaces", 2);
app.set("connection", connection);
app.set("padelConnection", padelConnection)

app.use(
  helmet({
    hsts: {
      maxAge: 31536000,
    },
    contentSecurityPolicy: {
      useDefaults: false,
      directives: {
        "default-src": ["'none'"],
        "frame-ancestors": ["'none'"],
      },
    },
    frameguard: {
      action: "deny",
    },
  })
);

app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

app.use((req, res, next) => {
  res.contentType("application/json; charset=utf-8");
  console.log("req:", req.url)
  next();
});

app.use(nocache());

app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "PUT", "DELETE"],
    maxAge: 86400,
  })
);

app.use(userRoutes);
app.use(collectionRoutes);
app.use(dataRoutes);
app.use(dvfRoutes);
app.use(padelRoutes);

app.use(errorHandler);
app.use(notFoundHandler);

const options = {
  key: fs.readFileSync('/etc/letsencrypt/live/app.idelia.org/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/app.idelia.org/cert.pem')
};

const server = https.createServer(options, app).listen(PORT, hostname, () => {
  const { address, port } = server.address();
  console.log(`Server listening on https://${address}:${port}`);
});
