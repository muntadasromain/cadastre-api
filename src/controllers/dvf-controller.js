const departements = require("../assets/departements.json");
const pako = require("pako");
const https = require("https");
const stream = require("stream");
const zlib = require("zlib");

const getDVFByYear = async (insee, year) => {
  try {

    return new Promise((resolve, reject) => {
      let codeInsee = insee + "";
      let dpt = codeInsee.substring(0, 2);
      if (dpt === "97") dpt = codeInsee.substring(0, 3);

      const url = `https://files.data.gouv.fr/geo-dvf/latest/csv/${year}/communes/${dpt}/${codeInsee}.csv`;

      https
        .get(url, (res) => {
          const { statusCode } = res;
          const contentType = res.headers["content-type"];

          let error;
          if (statusCode !== 200) {
            error = new Error("Request Failed.\n" + `Status Code: ${statusCode}`);
          } else if (!/^text\/csv/.test(contentType)) {
            error = new Error(
              "Invalid content-type.\n" +
              `Expected text/csv but received ${contentType}`
            );
          }
          if (error) {
            console.error(error.message);
            // Consume response data to free up memory
            res.resume();
            return;
          }

          // convert csv lines to json objects
          // first line of csv file is the header
          // each line is a transaction
          // each column is separated by a comma

          let data = [];
          let header = [];

          res.on("data", (chunk) => {
            let lines = chunk.toString().split("\n");
            for (let i = 0; i < lines.length; i++) {
              if (i === 0) {
                header = lines[0].split(",");
              } else {
                let line = lines[i].split(",");
                let obj = {};
                for (let j = 0; j < line.length; j++) {
                  obj[header[j]] = line[j];
                }
                data.push(obj);
              }
            }
          });

          res.on("end", () => {
            resolve(data);
          });
        })
        .on("error", (e) => {
          console.error(`Got error: ${e.message}`);
        });
    });
  } catch (error) {
    console.log(error);
  }
};

const getDVFByInsee = async (req, res) => {
  try {

    let years = [2018, 2019, 2020, 2021, 2022];
    // getDVFByYear for each year
    let dvf = [];
    for (let i = 0; i < years.length; i++) {
      let data = await getDVFByYear(req.params.insee, years[i]);
      dvf.push({ year: years[i], DVF: data });
    }
    res.status(200).send(dvf);
  } catch (error) {
    console.log(error);
  }
};

const getDVFByInseeByYear = async (req, res) => {
  try {
    let data = await getDVFByYear(req.params.insee, req.params.year);
    res.status(200).send(data);
  } catch (error) {
    console.log(error);
  }
};

const getDVFBySection = async (req, res) => {
  try {

    let years = [2018, 2019, 2020, 2021, 2022];
    // getDVFByYear for each year
    let dvf = [];
    for (let i = 0; i < years.length; i++) {
      let data = await getDVFByYear(req.params.plotId.substring(0, 5), years[i]);
      let foundPlots = data.filter(
        (d) =>
          d.id_parcelle &&
          d.id_parcelle.substring(9, 11) === req.params.plotId.substring(9, 11)
      );
      if (foundPlots.length > 0) dvf.push(foundPlots);
    }
    res.status(200).send(dvf);
  } catch (error) {
    console.log(error);
  }
};

const getDVFByPlotId = async (req, res) => {
  try {
    let years = [2018, 2019, 2020, 2021, 2022];
    // getDVFByYear for each year
    let dvf = [];
    for (let i = 0; i < years.length; i++) {
      let data = await getDVFByYear(req.params.plotId.substring(0, 5), years[i]);
      let foundPlots = data.filter((d) => d.id_parcelle === req.params.plotId);
      if (foundPlots.length > 0) dvf.push(foundPlots);
    }
    console.log(dvf);
    res.status(200).send(dvf);
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getDVFByYear,
  getDVFByInsee,
  getDVFByInseeByYear,
  getDVFByPlotId,
  getDVFBySection,
};
