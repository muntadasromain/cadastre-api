const departements = require("../assets/departements.json");
const pako = require("pako");
const https = require("https");
const stream = require("stream");
const zlib = require("zlib");
const { v4: uuidv4 } = require("uuid");

const fetchParcellesByInsee = async (insee) => {
  let codeInsee = insee + "";
  let dpt = codeInsee.substring(0, 2);
  if (dpt === "97") dpt = codeInsee.substring(0, 3);

  const plots = await fetch(
    "https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/communes/" +
      dpt +
      "/" +
      codeInsee +
      "/cadastre-" +
      codeInsee +
      "-parcelles.json.gz"
  )
    .then((response) => response.arrayBuffer())
    .then((buffer) => new Uint8Array(buffer))
    .then((gzipped) => pako.ungzip(gzipped, { to: "string" }))
    .then((unzipped) => JSON.parse(unzipped));

  return plots;
};

const fetchParcellesByDpt = async (dpt) => {
  const url = `https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/departements/${dpt}/cadastre-${dpt}-parcelles.json.gz`;
  const options = new URL(url);

  const gunzipStream = zlib.createGunzip();
  let jsonString = "";

  https.get(options, (res) => {
    res.pipe(gunzipStream);
  });

  gunzipStream.on("data", (chunk) => {
    jsonString += chunk.toString();
  });

  gunzipStream.on("end", () => {
    const plots = JSON.parse(jsonString);
    return plots;
  });

  gunzipStream.on("error", (err) => {
    console.log(err);
  });
};

const fetchBatiByInsee = async (insee) => {
  let dpt = insee.substring(0, 2);
  if (dpt === "97") dpt = insee.substring(0, 3);

  const response = await fetch(
    "https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/communes/" +
      dpt +
      "/" +
      insee +
      "/cadastre-" +
      insee +
      "-batiments.json.gz"
  )
    .then((response) => response.arrayBuffer())
    .then((buffer) => new Uint8Array(buffer))
    .then((gzipped) => pako.ungzip(gzipped, { to: "string" }))
    .then((unzipped) => JSON.parse(unzipped));

  return response;
};

const fetchPLUByInsee = async (insee) => {
  const response = await fetch(
    "https://apicarto.ign.fr/api/gpu/zone-urba?partition=DU_" + insee
  );
  const data = await response.json();
  return data;
};

const fetchSectionsByInsee = async (insee) => {
  try {
    const response = await fetch(
      "https://apicarto.ign.fr/api/cadastre/feuille?code_insee=" + insee
    );
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
};

const fetchCommuneByDepartement = async (dpt) => {
  const response = await fetch(
    "https://geo.api.gouv.fr/communes?codeDepartement=" +
      dpt +
      "&fields=nom,code,codeDepartement,codeRegion,codesPostaux,contour,bbox,centre&format=json&geometry=centre"
  );
  const data = await response.json();
  return data;
};

const synchronizeData = () => {
  departements.forEach(async (dpt, i) => {
    if (dpt.num_dep === "01") {
      const parcelles = await fetchParcellesByDpt(dpt.num_dep);
      console.log(parcelles);
    }
  });
};

module.exports = {
  fetchParcellesByInsee,
  fetchBatiByInsee,
  fetchPLUByInsee,
  fetchSectionsByInsee,
  fetchCommuneByDepartement,
  synchronizeData,
  fetchParcellesByDpt,
};
