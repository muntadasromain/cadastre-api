const { v4: uuidv4 } = require("uuid");

const getOwnCollections = (req, res) => {


  try {
    const connection = req.app.get("connection");
    const userUUID = req.authUser.uuid;

    connection.query(
      "SELECT DISTINCT * FROM collection, shared_with WHERE collection.author_uuid = '" +
      userUUID +
      "' AND shared_with.collection_uuid = collection.uuid AND shared_with.client_uuid = '" +
      userUUID +
      "'",
      (err, rows) => {
        if (err) {
          res.status(500).send("Error");
          console.log(err);
        } else {
          // for each collection, set the plots value to its JSON.parse() value
          let ownCollections = rows.map((coll) => {
            let ownColl = coll;
            ownColl.plots = JSON.parse(ownColl.plots);
            return ownColl;
          });

          res.status(200).json(ownCollections);
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const getSharedCollections = (req, res) => {

  try {

    const connection = req.app.get("connection");
    const userUUID = req.authUser.uuid;

    connection.query(
      "SELECT * FROM collection, shared_with WHERE collection.uuid = shared_with.collection_uuid AND shared_with.client_uuid = '" +
      userUUID +
      "' AND collection.author_uuid != '" +
      userUUID +
      "' ORDER BY shared_with.fav DESC",
      (err, rows) => {
        if (err) {
          res.status(500).send("Error");
        } else {
          // for each collection, set the plots value to its JSON.parse() value
          let sharedCollections = rows.map((coll) => {
            let sharedColl = coll;
            sharedColl.plots = JSON.parse(sharedColl.plots);
            return sharedColl;
          });

          res.status(200).json(sharedCollections);
        }
      }
    );

  } catch (error) {
    console.log(error);
  }

};

const getCollection = (req, res) => {

  try {

    const connection = req.app.get("connection");
    const collectionUUID = req.params.collectionUUID;

    let collection = null;
    let sharedWith = null;

    connection.query(
      "SELECT collection.uuid, collection.name, collection.description, collection.plots, collection.creation_date, shared_with.last_edited as userLastEdit, collection.last_edited as collectionLastEdit, collection.author_uuid, shared_with.can_read, shared_with.can_write, shared_with.fav FROM collection, shared_with WHERE collection.uuid = '" +
      collectionUUID +
      "' AND shared_with.collection_uuid = collection.uuid AND shared_with.client_uuid = '" +
      req.authUser.uuid +
      "'",
      (err, rows) => {
        if (err) {
          console.log("erreur 1er requete", err);
          res.status(500).send("Error");
        } else {
          if (rows.length === 0) {
            collection = {};
          } else {
            collection = rows[0];
          }
        }
      }
    );

    connection.query(
      "SELECT client.uuid, client.name, client.first_name, client.email, shared_with.can_read, shared_with.can_write, shared_with.fav, shared_with.last_edited FROM shared_with, client, collection " +
      "WHERE client.uuid = shared_with.client_uuid " +
      "AND collection.uuid = shared_with.collection_uuid " +
      "AND client.uuid != collection.author_uuid " +
      "AND collection.uuid = '" +
      collectionUUID +
      "'",
      (err, rows) => {
        if (err) {
          console.log("erreur 2eme requete", err);
          res.status(500).send("Error");
        } else {
          if (rows.length === 0) {
            sharedWith = [];
          } else {
            sharedWith = rows;
          }
        }
      }
    );

    while (collection === null || sharedWith === null) {
      require("deasync").runLoopOnce();
    }

    if (collection != {}) {
      collection.plots = JSON.parse(collection.plots);
      collection.sharedWith = sharedWith;
    }

    res.status(200).json(collection);

  } catch (error) {
    console.log(error);
  }
};

const createCollection = (req, res) => {

  try {

    const connection = req.app.get("connection");
    const userUUID = req.authUser.uuid;

    const name = req.body.name;
    const description = req.body.description;
    const plots = req.body.plots;
    const sharedWith = req.body.sharedWith;

    const collectionUUID = uuidv4();
    // date for mysql datetime type
    const creationDate = new Date().toISOString().slice(0, 19).replace("T", " ");

    console.log("Date OK: " + creationDate);

    connection.query(
      "INSERT INTO collection (uuid, name, description, plots, creation_date, author_uuid, last_edited) VALUES ('" +
      collectionUUID +
      "', '" +
      name +
      "', '" +
      description +
      "', '" +
      JSON.stringify(plots) +
      "', '" +
      creationDate +
      "', '" +
      userUUID +
      "', '" +
      creationDate +
      "')",
      (err, rows) => {
        if (err) {
          console.log(err);
          res.status(500).send(err);
        } else {
          console.log("insertion into collection table successful");
          connection.query(
            "INSERT INTO shared_with (collection_uuid, client_uuid, can_read, can_write, fav, last_edited) VALUES ('" +
            collectionUUID +
            "', '" +
            userUUID +
            "', '1', '1', '0', '" +
            creationDate +
            "')",
            (err, rows) => {
              if (err) {
                console.log(err);
                res.status(500).send("Error");
                return;
              }
              console.log("finished inserting into shared_with table");
            }
          );

          // insert into shared_with table
          sharedWith.forEach((client) => {
            connection.query(
              "INSERT INTO shared_with (collection_uuid, client_uuid, can_read, can_write, fav) VALUES ('" +
              collectionUUID +
              "', '" +
              client.uuid +
              "', '" +
              client.can_read +
              "', '" +
              client.can_write +
              "', '" +
              client.fav +
              "')",
              (err, rows) => {
                if (err) {
                  console.log(err);
                  res.status(501).send(err);
                  return;
                }
              }
            );
          });
          res.status(201).json({ uuid: collectionUUID });
          console.log("success");
        }
      }
    );

  } catch (error) {
    console.log(error);
  }

};

const updateCollection = (req, res) => {

  try {

    const connection = req.app.get("connection");

    const collectionUUID = req.params.collectionUUID;
    const name = req.body.name;
    const description = req.body.description;
    const plots = req.body.plots;
    const last_edited = new Date().toISOString().slice(0, 19).replace("T", " ");

    connection.query(
      "UPDATE collection SET name = '" +
      name +
      "', description = '" +
      description +
      "', plots = '" +
      JSON.stringify(plots) +
      "', last_edited = '" +
      last_edited +
      "' WHERE uuid = '" +
      collectionUUID +
      "'",
      (err, rows) => {
        if (err) {
          res.status(500).send("Error");
        } else {
          connection.query(
            "UPDATE shared_with SET last_edited = '" +
            last_edited +
            "' WHERE shared_with.collection_uuid = '" +
            collectionUUID +
            "' AND shared_with.client_uuid = '" +
            req.authUser.uuid +
            "'",
            (err, rows) => {
              if (err) {
                console.log(err);
                res.status(500).send("Error");
              } else {
                res.status(200).send("Success");
              }
            }
          );
        }
      }
    );

  } catch (error) {
    console.log(error);
  }

};

const updateCollectionUsers = (req, res) => {

  try {

    const connection = req.app.get("connection");

    const collectionUUID = req.params.collectionUUID;
    const shared_with = req.body.shared_with;

    console.log("utilisateurs : ", shared_with);

    // delete all shared_with entries for this collection
    connection.query(
      "DELETE FROM shared_with WHERE collection_uuid = '" +
      collectionUUID +
      "' AND shared_with.client_uuid != '" +
      req.authUser.uuid +
      "'",
      (err, rows) => {
        if (err) {
          console.log(err);
          res.status(500).send("Error");
        } else {
          let success = true; // flag variable

          if (shared_with.length > 0) {
            // insert into shared_with table
            shared_with.forEach((client, index) => {
              connection.query(
                "INSERT INTO shared_with (collection_uuid, client_uuid, can_read, can_write, fav, last_edited) VALUES ('" +
                collectionUUID +
                "', '" +
                client.uuid +
                "', '" +
                client.can_read +
                "', '" +
                client.can_write +
                "', '" +
                client.fav +
                "', '" +
                new Date(client.last_edited)
                  .toISOString()
                  .slice(0, 19)
                  .replace("T", " ") +
                "')",
                (err, rows) => {
                  if (err) {
                    console.log(err);
                    success = false; // set flag variable to false
                  }

                  if (index === shared_with.length - 1) {
                    // last iteration of forEach loop
                    if (success) {
                      console.log("success after insert");
                      res.status(200).send("Success");
                    } else {
                      res.status(500).send("Error"); // send error response once
                    }
                  }
                }
              );
            });
          } else {
            res.status(200).send("Success with no shared_with");
          }
        }
      }
    );

  } catch (error) {
    console.log(error);
  }

};

const updateCollectionName = (req, res) => {

  try {

    const connection = req.app.get("connection");

    const collectionUUID = req.params.collectionUUID;
    const name = req.body.name;
    const last_edited = new Date().toISOString().slice(0, 19).replace("T", " ");

    connection.query(
      "UPDATE collection SET name = '" +
      name +
      "', last_edited = '" +
      last_edited +
      "' WHERE uuid = '" +
      collectionUUID +
      "'",
      (err, rows) => {
        if (err) {
          res.status(500).send("Error");
        } else {
          connection.query(
            "UPDATE shared_with SET last_edited = '" +
            last_edited +
            "' WHERE shared_with.collection_uuid = '" +
            collectionUUID +
            "' AND shared_with.client_uuid = '" +
            req.authUser.uuid +
            "'",
            (err, rows) => {
              if (err) {
                res.status(500).send("Error");
              } else {
                res.status(200).send("Success");
              }
            }
          );
        }
      }
    );

  } catch (error) {
    console.log(error);
  }

};

const updateCollectionDescription = (req, res) => {

  try {

    const connection = req.app.get("connection");

    const collectionUUID = req.params.collectionUUID;
    const description = req.body.description;
    const last_edited = new Date().toISOString().slice(0, 19).replace("T", " ");

    connection.query(
      "UPDATE collection SET description = '" +
      description +
      "', last_edited = '" +
      last_edited +
      "' WHERE uuid = '" +
      collectionUUID +
      "'",
      (err, rows) => {
        if (err) {
          res.status(500).send("Error");
        } else {
          connection.query(
            "UPDATE shared_with SET last_edited = '" +
            last_edited +
            "' WHERE shared_with.collection_uuid = '" +
            collectionUUID +
            "' AND shared_with.client_uuid = '" +
            req.authUser.uuid +
            "'",
            (err, rows) => {
              if (err) {
                res.status(500).send("Error");
              } else {
                res.status(200).send("Success");
              }
            }
          );
        }
      }
    );

  } catch (error) {
    console.log(error);
  }
};

const updateCollectionFav = (req, res) => {
  try {

    const connection = req.app.get("connection");

    const collectionUUID = req.params.collectionUUID;
    const fav = req.body.fav;
    const last_edited = new Date().toISOString().slice(0, 19).replace("T", " ");

    connection.query(
      "UPDATE shared_with SET fav = '" +
      fav +
      "' WHERE collection_uuid = '" +
      collectionUUID +
      "' AND client_uuid = '" +
      req.authUser.uuid +
      "'",
      (err, rows) => {
        if (err) {
          console.log(err);
          res.status(500).send("Error");
        } else {
          connection.query(
            "UPDATE shared_with SET last_edited = '" +
            last_edited +
            "' WHERE shared_with.collection_uuid = '" +
            collectionUUID +
            "' AND shared_with.client_uuid = '" +
            req.authUser.uuid +
            "'",
            (err, rows) => {
              if (err) {
                console.log(err);
                res.status(500).send("Error");
              } else {
                res.status(200).send("Success");
              }
            }
          );
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const deleteCollection = (req, res) => {
  try {

    const connection = req.app.get("connection");
    const collectionUUID = req.params.collectionUUID;

    // delete collection and associated shared_with entries
    connection.query(
      "DELETE FROM collection WHERE uuid = '" + collectionUUID + "'",
      (err, rows) => {
        if (err) {
          res.status(500).send("Error");
        } else {
          connection.query(
            "DELETE FROM shared_with WHERE collection_uuid = '" +
            collectionUUID +
            "'",
            (err, rows) => {
              if (err) {
                res.status(500).send("Error");
              } else {
                res.status(200).send("Success");
              }
            }
          );
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getOwnCollections,
  getSharedCollections,
  getCollection,
  createCollection,
  updateCollection,
  deleteCollection,
  updateCollectionUsers,
  updateCollectionName,
  updateCollectionDescription,
  updateCollectionFav,
};
