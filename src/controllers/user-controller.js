const { v4: uuidv4 } = require("uuid");

const getUserBySub = async (req, res, sub) => {
  try {
    const connection = req.app.get("connection");

    const result = await new Promise((resolve, reject) => {
      connection.query(
        "SELECT * FROM client WHERE client.sub = '" + sub + "'",
        (err, rows) => {
          if (err) {
            console.error("Error in getUserBySub cnnection.query():", err);
            reject(err);
          } else {
            if (rows.length === 0) resolve(null);
            else resolve(JSON.parse(JSON.stringify(rows[0])));
          }
        }
      );
    });
    return result;
  } catch (error) {
    console.error("Error in getUserBySub:", error);
  }
};

const getUserInfos = (req, res) => {

  try {

    const connection = req.app.get("connection");
    const userUUID = req.params.userUUID;

    connection.query(
      "SELECT name, first_name, email FROM client WHERE client.uuid = '" +
      userUUID +
      "'",
      (err, rows) => {
        if (err) {
          res.status(500).send("Error");
        } else {
          console.log("rows[0]:", rows[0]);
          console.log(
            "JSON.parse(JSON.stringify(rows[0])):",
            JSON.parse(JSON.stringify(rows[0]))
          );
          res.status(200).json(JSON.parse(JSON.stringify(rows[0])));
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const getAllUsers = (req, res) => {
  try {

    const connection = req.app.get("connection");
    const userUUID = req.authUser.uuid;

    connection.query(
      "SELECT uuid, name, first_name, email FROM client WHERE client.uuid != '" +
      userUUID +
      "'",
      (err, rows) => {
        if (err) {
          res.status(500).send("Error");
        } else {
          res.status(200).json(JSON.parse(JSON.stringify(rows)));
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const checkUser = (req, res) => {

  try {

    const connection = req.app.get("connection");

    connection.query(
      "SELECT * FROM client WHERE client.sub = '" + req.body.sub + "'",
      (err, rows) => {
        if (err) {
          res.status(500).send("Error");
        } else {
          if (rows.length > 0) {
            res.status(200).json("User already exists");
          } else {
            res.status(200).json("Not found");
          }
        }
      }
    );

  } catch (error) {
    console.log(error);
  }
};

const createAccount = (req, res) => {
  try {

    const connection = req.app.get("connection");
    const sub = req.body.sub;
    const name = req.body.name;
    const first_name = req.body.first_name;
    const email = req.body.email;
    const uuid = uuidv4();
    const creationDate = new Date().toISOString().slice(0, 19).replace("T", " ");

    connection.query(
      "INSERT INTO client (uuid, sub, name, first_name, email, creation_date) VALUES ('" +
      uuid +
      "', '" +
      sub +
      "', '" +
      name +
      "', '" +
      first_name +
      "', '" +
      email +
      "', '" +
      creationDate +
      "')",
      (err, rows) => {
        if (err) {
          console.log("err", err);
          res.status(500).send("Error");
        } else {
          res.status(200).json("Account created");
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getUserBySub,
  getUserInfos,
  getAllUsers,
  checkUser,
  createAccount,
};
